import 'package:flutter/material.dart';

void main() {
  runApp(AlignExample());
}

class AlignExample extends StatelessWidget {
  const AlignExample ({Key? key}) :super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Center(
          child: Container(
            height: 200.0,
            width: 600.0,
            color: Colors.green[200],
            child: Align(
              alignment: const Alignment(-0.75, -0.75),
              child: Container(
                color: Colors.green[200],
                child: const Text('Align me !'),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

