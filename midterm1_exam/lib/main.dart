import 'package:flutter/material.dart';
import 'package:midterm1_exam/screen/Login.dart';
import 'package:midterm1_exam/screen/home.dart';
import 'package:midterm1_exam/screen/timetable.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Home',
      theme: ThemeData(
        primarySwatch: Colors.grey,
      ),
      home:HomeScreen()
    );
  }
}

