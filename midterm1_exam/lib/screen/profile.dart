import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreen createState() => _ProfileScreen();
}

class _ProfileScreen extends State<ProfileScreen> {
  final double coverHeing = 280;
  final double profileHeingt = 144;
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      appBar: AppBar(
        title: Text("ประวัตินิสิต"),
      ),
      body:ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          buildTop(),
          buildContent(),
        ]
        
      )
      
    );
  }
  Widget buildTop() {
    final bottom = profileHeingt /2;
    final top = coverHeing - profileHeingt /2;
    return Stack(
        clipBehavior: Clip.none,
        alignment: Alignment.center ,
        children:[
          Container(
            margin: EdgeInsets.only(bottom: bottom),
            child: buildCoverImage(),
          ),
          
          Positioned(
            top: top,
            child:buildProfileImage(), 
            ),
          
        ]
    );
}
  Widget buildCoverImage() => Container(
    color: Colors.grey,
    child: Image.network('https://www.google.com/url?sa=i&url=https%3A%2F%2Fcode.likeagirl.io%2Fwhy-i-code-and-why-you-should-too-1dcfe1efc5a8&psig=AOvVaw2xUo6I8Nmk3XoZCQm6liGu&ust=1675337779650000&source=images&cd=vfe&ved=0CBAQjRxqFwoTCLii2dyd9PwCFQAAAAAdAAAAABAE'),
    width: double.infinity,
    height: coverHeing,
     
  ) ;
   Widget buildProfileImage() => CircleAvatar(
    radius: profileHeingt / 2 ,
    backgroundColor: Colors.grey.shade800,
    backgroundImage: NetworkImage(
      'https://scontent.futp1-2.fna.fbcdn.net/v/t39.30808-6/298768917_1483270792134651_3478011752516117099_n.jpg?_nc_cat=110&ccb=1-7&_nc_sid=09cbfe&_nc_eui2=AeEQvTGPRBx0GSaJQR_wwGZc8LkCOy5-mMnwuQI7Ln6YyTC-vTG1wHt4yW_rI3iG1v37VXe4imNyZsu8QbSdsl-e&_nc_ohc=vWp-hK3eovYAX_llohQ&tn=yz7hl7vGBubhHf43&_nc_ht=scontent.futp1-2.fna&oh=00_AfDa5XLzBRmGCCeCND9xXqO8eGzw5-KCVGpqMJ6h4-YXBg&oe=63DFE5D8'),
  );
  
  Widget buildContent() => Column(
    children: [
      const SizedBox(height: 8),
      Text('63160007',style: TextStyle(fontSize: 30),
      ),
      const SizedBox(height: 8),
      Text('Nonnanee Rodma',style: TextStyle(fontSize: 28),
      ),
      const SizedBox(height: 8),
      Text('นนณี รอดมา',style: TextStyle(fontSize: 20),
      ),
      Padding(
        padding: const EdgeInsets.only(top:20.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Column(
          children: const [
            Text(
              "3",
              style: TextStyle(
                color: Colors.grey,
                fontSize: 18,
                fontWeight: FontWeight.bold,
                
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              "ชั้นปี",
              style: TextStyle(
                color: Colors.grey,
                fontSize: 16,
              ),
            )
          ],
        ),
        Column(
          children: const [
            Text(
              "18",
              style: TextStyle(
                color: Colors.grey,
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              "หน่วยกิต",
              style: TextStyle(
                color: Colors.grey,
                fontSize: 16,
              ),
            )
          ],
        ),
        Column(
          children: const [
            Text(
              "2.17",
              style: TextStyle(
                color: Colors.grey,
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              "GPA",
              style: TextStyle(
                color: Colors.grey,
                fontSize: 16,
              ),
            )
          ],
        )
          ],),
      ),
      Text('ข้อมูลด้านการศึกษา',style: TextStyle(fontSize: 20),
      ),
          
     Padding(
        padding: const EdgeInsets.only(top:20.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            const SizedBox(height: 8),
      
            Column(
          children: const [
            Text(
              "รหัสประจำตัว:",
              
              style: TextStyle(
                color: Colors.grey,
                fontSize: 10,
                fontWeight: FontWeight.bold,
                
              ),
            ),
            SizedBox(
              height: 5,
            ),
             Text(
              "เลขที่บัตรประชาชน:",
              
              style: TextStyle(
                color: Colors.grey,
                fontSize: 10,
                fontWeight: FontWeight.bold,
                
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              "ชื่ออังกฤษ:",
              
              style: TextStyle(
                color: Colors.grey,
                fontSize: 10,
                fontWeight: FontWeight.bold,
                
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              "คณะ:",
              
              style: TextStyle(
                color: Colors.grey,
                fontSize: 10,
                fontWeight: FontWeight.bold,
                
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              "วิทยาเขต:",
              
              style: TextStyle(
                color: Colors.grey,
                fontSize: 10,
                fontWeight: FontWeight.bold,
                
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              "หลักสูตร:",
              
              style: TextStyle(
                color: Colors.grey,
                fontSize: 10,
                fontWeight: FontWeight.bold,
                
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              "วิชาโท:",
              
              style: TextStyle(
                color: Colors.grey,
                fontSize: 10,
                fontWeight: FontWeight.bold,
                
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              "ระดับการศึกษา:",
              
              style: TextStyle(
                color: Colors.grey,
                fontSize: 10,
                fontWeight: FontWeight.bold,
                
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              "ชื่อปริญญา:",
              
              style: TextStyle(
                color: Colors.grey,
                fontSize: 10,
                fontWeight: FontWeight.bold,
                
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              "ปีการศึกษาที่เข้า:",
              
              style: TextStyle(
                color: Colors.grey,
                fontSize: 10,
                fontWeight: FontWeight.bold,
                
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              "สถานะภาพ:",
              
              style: TextStyle(
                color: Colors.grey,
                fontSize: 10,
                fontWeight: FontWeight.bold,
                
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              "วิธีรับเข้า:",
              
              style: TextStyle(
                color: Colors.grey,
                fontSize: 10,
                fontWeight: FontWeight.bold,
                
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              "วุฒิก่อนเข้ารับการศึกษา:",
              
              style: TextStyle(
                color: Colors.grey,
                fontSize: 10,
                fontWeight: FontWeight.bold,
                
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              "จบศึกษาจาก:",
              
              style: TextStyle(
                color: Colors.grey,
                fontSize: 10,
                fontWeight: FontWeight.bold,
                
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              "อ.ที่ปรึกษา:",
              
              style: TextStyle(
                color: Colors.grey,
                fontSize: 10,
                fontWeight: FontWeight.bold,
                
              ),
            ),
            SizedBox(
              height: 5,
            ),
          ],
        ),
       Column(
          children: const [
            Text(
              "63160007",
              
              style: TextStyle(
                color: Colors.grey,
                fontSize: 10,
                fontWeight: FontWeight.bold,
                
              ),
            ),
            SizedBox(
              height: 5,
            ),
             Text(
              "2200101114936",
              
              style: TextStyle(
                color: Colors.grey,
                fontSize: 10,
                fontWeight: FontWeight.bold,
                
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              "MISS NONNANEE RODMA",
              
              style: TextStyle(
                color: Colors.grey,
                fontSize: 10,
                fontWeight: FontWeight.bold,
                
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              "คณะวิทยาการสารสนเทศ",
              
              style: TextStyle(
                color: Colors.grey,
                fontSize: 10,
                fontWeight: FontWeight.bold,
                
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              "บางแสน",
              
              style: TextStyle(
                color: Colors.grey,
                fontSize: 10,
                fontWeight: FontWeight.bold,
                
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              "2115020 วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ",
              
              style: TextStyle(
                color: Colors.grey,
                fontSize: 10,
                fontWeight: FontWeight.bold,
                
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              "-",
              
              style: TextStyle(
                color: Colors.grey,
                fontSize: 10,
                fontWeight: FontWeight.bold,
                
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              "ปริญญาตรี",
              
              style: TextStyle(
                color: Colors.grey,
                fontSize: 10,
                fontWeight: FontWeight.bold,
                
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              "วิทยาศาสตรบัณฑิต วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ",
              
              style: TextStyle(
                color: Colors.grey,
                fontSize: 10,
                fontWeight: FontWeight.bold,
                
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              "2563 / 1 วันที่ 5/2/2563",
              
              style: TextStyle(
                color: Colors.grey,
                fontSize: 10,
                fontWeight: FontWeight.bold,
                
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              " ",
              
              style: TextStyle(
                color: Colors.grey,
                fontSize: 10,
                fontWeight: FontWeight.bold,
                
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              "โครงการความร่วมมือทางวิชาการ (MOU)",
              
              style: TextStyle(
                color: Colors.grey,
                fontSize: 10,
                fontWeight: FontWeight.bold,
                
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              "ม.6 2.68",
              
              style: TextStyle(
                color: Colors.grey,
                fontSize: 10,
                fontWeight: FontWeight.bold,
                
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              "หัวถนนวิทยา",
              
              style: TextStyle(
                color: Colors.grey,
                fontSize: 10,
                fontWeight: FontWeight.bold,
                
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              "ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน,อาจารย์ภูสิต กุลเกษม",
              
              style: TextStyle(
                color: Colors.grey,
                fontSize: 10,
                fontWeight: FontWeight.bold,
                
              ),
            ),
            SizedBox(
              height: 5,
            ),
          ],
        ),
          ],),
      ),

    ],
  );
}



 
