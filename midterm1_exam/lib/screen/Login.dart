import 'package:flutter/material.dart';
import 'package:midterm1_exam/screen/profile.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreen createState() => _LoginScreen();
}

class _LoginScreen extends State<LoginScreen> {
  TextEditingController name = TextEditingController();
  TextEditingController password = TextEditingController();

  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      appBar: AppBar(title: Text("เข้าสู่ระบบ"),),
      body: Padding(
        padding: EdgeInsets.all(10),
        child: ListView(
          children: <Widget>[
            
            Container(
              alignment: Alignment.center,
              padding: EdgeInsets.all(10),
              child: Text(
                "Login",
                style: TextStyle(
                    color: Colors.blueGrey,
                    fontWeight: FontWeight.w500,
                    fontSize: 30),
              ),
            ),
            Image.asset("assets/images/Buu-logo11.png",width: 200,height: 200,),
            Container(
              alignment: Alignment.center,
              padding: EdgeInsets.all(10),
              child: Text(
                "Sing in",
                style: TextStyle(
                    color: Colors.blueGrey,
                    fontWeight: FontWeight.w500,
                    fontSize: 20),
              ),
            ),
            Container(
              alignment: Alignment.center,
              padding: EdgeInsets.all(10),
              child: TextField(
                controller: name,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: "รหัสนิสิต",
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
              child: TextField(
                obscureText: true,
                controller: password,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: "รหัสผ่าน",
                  
                ),
              ),
            ),
            const SizedBox(height: 20.0),
            ElevatedButton(
                        onPressed: () {
                          if (name.text.isEmpty ||
                              password.text.isEmpty) {
                            showDialog(
                                context: context,
                                builder: (context) {
                                  return AlertDialog(
                                    title: const Text("Error"),
                                    content: name.text.isEmpty &&
                                        password.text.isEmpty
                                        ? const Text(
                                        "กรุณากรอก username และ password")
                                        : name.text.isEmpty
                                        ? const Text("กรุณากรอก username")
                                        : const Text("กรุณากรอก password"),
                                    actions: [
                                      TextButton(
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                          },
                                          child: const Text("ตกลง")),
                                    ],
                                  );
                                });
                          }else {
                            Navigator.push(context, MaterialPageRoute(
                        builder: (context){
                          return ProfileScreen();
                          }),
                            );
                          }
                        },
                       
                        child: const Text('เข้าสู่ระบบ' ,style: TextStyle(fontSize: 20))),
                        
              
          ],
        ),
      ),
    );
  }
}


